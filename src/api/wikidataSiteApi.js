import axios from 'axios';

export default axios.create({
    baseURL: 'https://test.wikidata.org/w'
});
  