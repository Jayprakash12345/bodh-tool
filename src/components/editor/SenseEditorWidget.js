import React, { useState, useRef } from 'react';

import { Typeahead } from 'react-typeahead';

// API module import 
import wdSiteApi from '../../api/wikidataSiteApi';
import backendApi from '../../api/backendApi';

// Helper function
import {
    getTypeByString
} from '../../api/wikiApi';

function SenseEditorWidget( { proId, proType, senseRowData, setSenseRowData } ) {
    console.log(senseRowData)
    // Typehead states
    const typeheadRef = useRef();
    const [ options, setOptions ] = useState([])
    const [ value, setValue ] = useState( '' )

    const addClaimInRow = ( proId, data) => {
        let tempData = []
        try {
            tempData = [
                ...senseRowData.claims[proId],
                data
            ]
        } catch {
            tempData = [ data ]
        }
        setSenseRowData( {
            ...senseRowData,
            claims: {
                ...senseRowData.claims,
                [proId]: tempData
            }
        } )
    }

    const onTypheadKeyPress = (e) => {
        setValue( e.target.value )
        if( proType === 'wikibase-lexeme' || proType === 'wikibase-item' ){
            const tp = proType === 'wikibase-lexeme' ? 'lexeme' : 'item'
            wdSiteApi.get( 
                '/api.php?action=wbsearchentities&format=json&language=en&type='+tp+'&origin=*&search='+ e.target.value
            )
            .then( ( { data } ) => {
                setOptions(data.search)
            })
        } else if( proType === 'string' && e.key === 'Enter' ){
            createClaim(proId, e.target.value, proType);
        }
    }

    const onOptionSelected = ( op, proId ) => {
        const type = getTypeByString( op.id )
        createClaim(proId, op.id, type);
    }

    const createClaim = (proId, val, type) => {
        document.body.style.cursor='wait';
        backendApi.post(
            '/api/createclaim',
            {
                "entity": it.id,
                "property": proId,
                "value": val,
                "type": type
            },
            {
                crossDomain: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(({ data }) => {
                if (data["success"]) {
                    addClaimInRow(proId, data.claim);
                    typeheadRef.current.setEntryText('');
                } else {
                    alert("Failed to add the item :(");
                }
                document.body.style.cursor='default';
            })
            .catch(err => {
                document.body.style.cursor='default';
                alert("Failed to add the item :(")
            });
    }

    return (
        <Typeahead 
            style={{marginTop: 2}}
            options={options}
            filterOption='label'
            maxVisible={10}
            onKeyUp={onTypheadKeyPress}
            value={value}
            displayOption={ (op) => op.label + ' (' + op.id + ')'}
            onOptionSelected={(op) => onOptionSelected(op, proId)}
            ref={typeheadRef}
        />
    )
}

export default SenseEditorWidget;