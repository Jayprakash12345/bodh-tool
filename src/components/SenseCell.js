import React, { useState } from 'react';

import backendApi from './../api/backendApi';

import PropertyWidget from './PropertyWidget';
import SenseEditorWidget from './editor/SenseEditorWidget';

function SenseCell( { data, properties } ) {
    const [ rowData, setRowData ] = useState( data );

    const [ newItemLang, setNewItemLang ] = useState('');
    const [ newItemValue, setNewItemValue ] = useState('');

    /**
     * Handler to add new sense
     * 
     * @param {Event} e HTML input event
     */
    const handleNewSense = ( e ) => {
        if( e.key === 'Enter'){
            e.preventDefault()
            document.body.style.cursor= 'wait';
            backendApi.post(
                '/api/createsense',
                {
                    "lexemeId": rowData[0]["id"].split("-")[0],
                    "lang": newItemLang,
                    "value": newItemValue
                },
                {
                    crossDomain: true,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                })
                .then(({ data }) => {
                    if( data["success"] ){
                        const dataFromResp = {
                            id: data["sense"]["id"],
                            glosses: {
                                newItemLang: {
                                    language: data["sense"]["glosses"][Object.keys(data["sense"]["glosses"])[0]]["language"],
                                    value: data["sense"]["glosses"][Object.keys(data["sense"]["glosses"])[0]]["value"]
                                }
                            },
                            claims: []
                        }
                        setRowData( [...rowData, dataFromResp ] );

                        // Reset the data in new fields
                        setNewItemLang('')
                        setNewItemValue('')
                    } else {
                        alert("Failed to add the sense :(")
                    }
                    document.body.style.cursor='default';
                })
                .catch( () => {
                    document.body.style.cursor='default';
                    alert("Failed to add the sense :(")
                })
        }
    }

    /**
     * Returns header content
     * 
     * @param {object} pro Property Object 
     */
    const getPropsHeaderName = ( pro ) => {
        try {
            return <p style={{padding:0, margin: 0}}>{pro.id}<span className="pId"> ({pro.text}) </span> </p>
        } catch {
            return pro.id
        }
    }

    /**
     * Generate sense cell data
     * 
     * @param {string} proId PropertyID
     * @param {string} proType PropertyType like 'wikidata-item'
     * @param {object} item 
     */
    const getPropsDataCell = (proId, proType, item) => {
        return(
            <td> 
                { 
                    item.claims.length === undefined && item.claims[proId] ? 
                    item.claims[proId].map( (i) => {
                        return( <tr><PropertyWidget d={i} /></tr>)
                    })
                    : null 
                }
                <SenseEditorWidget 
                    style={{marginTop: 2}}
                    proId={proId} 
                    proType={proType}
                    senseRowData={rowData}
                    setSenseRowData={setRowData}
                />
            </td>
        )
    }

    /**
     * Delete sense from lexeme
     * 
     * @param {sting} id Sense id like L123-S1
     */
    const deleteSense = (id) => {
        backendApi.post(
            '/api/deletesense', 
            {
                "itemId": id
            },
            {
                crossDomain: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        )
        .then( ({ data }) => {
            if( data["success"] ){
                let tempData = rowData.filter( obj => obj.id != id )
                setRowData( tempData );
            } else {
                alert("Failed to delete the sense :(")
            }
        })
        .catch( err => alert("Failed to delete the sense :(") )
    }

    return (
        <>
            { properties.length > 0 ?
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    {properties.map( (pro, index) => {
                        return <td>{getPropsHeaderName(pro)}</td>
                    })}
                </tr>
                :
                null
            }
            {rowData.map( function(item, index) {
                return([
                    <tr key={index}>
                        <td>
                            {item.id.split('-')[1]}
                            <b style={{marginLeft: 3, cursor: 'pointer', color: 'red'}} onClick={() => deleteSense(item.id)}>✘</b>
                        </td>
                        <td>
                            {item.glosses[Object.keys(item.glosses)[0]].language}
                        </td>
                        <td>
                            {item.glosses[Object.keys(item.glosses)[0]].value}
                        </td>
                        {properties.map( (pro) => getPropsDataCell(pro.id, pro.type, item) )}
                    </tr>
                ])
            })}
            {
            <tr key="new">
                <td></td>
                <td><input type="text" placeholder="Lang" size={5} value={newItemLang} onChange={ (e) => setNewItemLang(e.target.value)} /></td>
                <td><input type="text" placeholder="Value" value={newItemValue} onChange={ (e) => setNewItemValue(e.target.value)} onKeyPress={handleNewSense} /></td>
            </tr>
            }
        </>
    );
}

export default SenseCell;