import React, { useState } from 'react';

import PropertyWidget from './PropertyWidget';

function FormsCell( { data, properties } ) {
    const [ rowData, setRowData ] = useState( data );

    const [ newItemLang, setNewItemLang ] = useState('');
    const [ newItemValue, setNewItemValue ] = useState('');

    const handleNewItem = ( e ) => {
        if( e.key === 'Enter'){
            e.preventDefault()

            fetch('/api/createform', {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    lexemeId: rowData[0]["id"].split("-")[0],
                    lang: newItemLang,
                    value: newItemValue
                })
            })
            .then(response => response.json())
            .then(data => { 
                if( data["success"] ){
                    const dataFromResp = {
                        id: data["form"]["id"],
                        representations: {
                            newItemLang: {
                                language: data["form"]["representations"][Object.keys(data["form"]["representations"])[0]]["language"],
                                value: data["form"]["representations"][Object.keys(data["form"]["representations"])[0]]["value"]
                            }
                        },
                        claims: [],
                        grammaticalFeatures: []
                    }

                    setRowData( [...rowData, dataFromResp ] );

                    // Reset the data in new fields
                    setNewItemLang('')
                    setNewItemValue('')
                }
            })
        }
    }

    const getPropsHeaderName = ( pro ) => {
        try {
            return <p style={{padding:0, margin: 0}}>{pro.id}<span className="pId"> ({pro.text}) </span> </p>
        } catch {
            return pro.id
        }
    }

    const getPropsDataCell = (pro, item) => {
        return(
            <td>
                { 
                    item.claims !== undefined && item.claims[pro] ? 
                    item.claims[pro].map( (i) => {
                        console.log( i)
                        return( <tr><PropertyWidget d={i} /></tr>)
                    })
                    : null 
                }
                <input style={{marginTop: 2}} type="text" placeholder="Value" />
            </td>
        )
    }

    return (
        <>
            { properties.length > 0 ?
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    {properties.map( (pro, index) => {
                        return <td>{getPropsHeaderName(pro)}</td>
                    })}
                </tr>
                :
                null
            }
            {rowData.map( function(item, index) {
                return([
                    <tr key={index}>
                        <td>{item.id.split('-')[1]}</td>
                        <td>
                            {item.representations[Object.keys(item.representations)[0]].language}
                        </td>
                        <td>
                            {item.representations[Object.keys(item.representations)[0]].value}
                        </td>
                        {properties.map( (pro) => getPropsDataCell(pro.id, item) )}
                    </tr>
                ])
            })}
            {
            <tr key="new">
                <td></td>
                <td><input type="text" placeholder="Lang" size={5} value={newItemLang} onChange={ (e) => setNewItemLang(e.target.value)} /></td>
                <td><input type="text" placeholder="Value" value={newItemValue} onChange={ (e) => setNewItemValue(e.target.value)} onKeyPress={handleNewItem} /></td>
            </tr>
            }
        </>
    );
}

export default FormsCell;