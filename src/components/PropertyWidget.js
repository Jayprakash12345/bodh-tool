import React, {useState, useEffect} from 'react';

// Component imports
import wdSiteApi from './../api/wikidataSiteApi';
import backendApi from './../api/backendApi';
import { Typeahead } from 'react-typeahead';


const PropertyWidget = ( { d, it, setIt, proId } ) => {
    let item = d.mainsnak.datavalue.value
    const itemType = d.mainsnak.datatype
    item = itemType === "wikibase-lexeme" ? item.id : item
    item = itemType === "wikibase-item" ? item.id : item
    const [itemText, setItemText ] = useState(item)
    const [itemValue, setItemValue ] = useState(item)
    const [ editMode, setEditMode ] = useState(false)

    // -----------   Typehead ------------
    const [ options, setOptions ] = useState([])

    const onTypheadKeyPress = (e) => {
        if( itemType === 'wikibase-lexeme' || itemType === 'wikibase-item' ){
            const t = itemType === 'wikibase-lexeme' ? 'lexeme' : 'item'
            wdSiteApi.get( 
                '/api.php?action=wbsearchentities&format=json&language=en&type='+t+'&origin=*&search='+ e.target.value
            )
            .then( ( { data } ) => {
                setOptions(data.search)
            })
        }
    }

    const onOptionSelected = ( op ) => {
        backendApi.post(
            '/api/editclaim', 
            {
                "claimId": d.id,
                "claimType": itemType,
                "value": op.id
            },
            {
                crossDomain: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        )
        .then( ({ data }) => {
            if( data["success"] ){
                setItemValue(op.id)
                setEditMode(false)
                setItemText(op.label)
            } else {
                alert("Failed to edit the item :(")
            }
        })
        .catch( err => alert("Failed to edit the item :(") )
    }

    const deleteItem = (id) => {
        backendApi.post(
            '/api/deleteitem', 
            {
                "itemId": id
            },
            {
                crossDomain: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        )
        .then( ({ data }) => {
            if( data["success"] ){
                let tempData = it.claims[proId].filter( obj => obj.id != id )
                const d = {
                    ...it,
                    claims: {
                        ...it.claims,
                        [proId]: tempData
                    }
                }
                setIt(d)
            } else {
                alert("Failed to delete the item :(")
            }
        })
        .catch( err => alert("Failed to delete the item :(") )
    }

    const handleItemValue = (e) => {
        setItemValue(e.target.value)
    }

    const handleItemEdit = ( e ) => {
        if( e.key === 'Enter'){
            e.preventDefault()
            backendApi.post(
                    '/api/editclaim', 
                    {
                        "claimId": d.id,
                        "claimType": itemType,
                        "value": itemValue
                    },
                    {
                        crossDomain: true,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        }
                    }
                )
                .then( ({ data }) => {
                    if( data["success"] ){
                        setItemValue(itemValue)
                        setEditMode(false)
                        // Follow up request to get text
                        wdSiteApi.get('/api.php?action=wbgetentities&format=json&origin=*&ids=' + itemValue )
                            .then(( { data } ) => {
                                const d1 = data.entities[itemValue]
                                if( d1.type === "lexeme"){
                                    setItemText( d1.lemmas[Object.keys(d1.lemmas)].value )
                                } else if ( d1.type === "item"){
                                    setItemText(d1.labels.en.value)
                                }
                            } )
                            .catch( () => alert(`Failed to get text for ${item} item`) )
                    } else {
                        alert("Failed to edit the item :(")
                    }
                })
                .catch( err => alert("Failed to edit the item :(") )
        }
    }

    useEffect( () => {
        if( itemType === "wikibase-item" || itemType === "wikibase-lexeme"){
            wdSiteApi.get('/api.php?action=wbgetentities&format=json&origin=*&ids=' + item )
                .then(( { data } ) => {
                    const d1 = data.entities[item]
                    if( d1.type === "lexeme"){
                        setItemText( d1.lemmas[Object.keys(d1.lemmas)].value )
                    } else if ( d1.type === "item"){
                        console.log( d1.id, d1.labels.en.value )
                        setItemText(d1.labels.en.value)
                    }
                } )
                .catch( () => alert(`Failed to get ${item} item`) )
                
        }
    }, [])
    const getCellContent = () => {
        if( itemType === "commonsMedia"){
            const link = "https://commons.wikimedia.org/wiki/File:" + item
            return <>
                <a href={link}>{item}</a>
            </>
        } else if (
                itemType === "string" || 
                itemType === "wikibase-item" || 
                itemType === "wikibase-lexeme"
            ){
            return <>
                <input disabled value={itemText}/>
                <div style={{marginLeft: -30, display: 'inline', zIndex:999}}>
                    <b style={{marginRight: 3, cursor: 'pointer'}} onClick={() => setEditMode(true)}>&#9998;</b>
                    <b style={{marginRight: 3, cursor: 'pointer', color: 'red'}} onClick={() => deleteItem(d.id)}>✘</b>
                </div>
            </>
        }
    }
    return(
        <>
            <div>
                {editMode === false ?
                    getCellContent()
                : <Typeahead 
                        style={{marginTop: 2}}
                        options={options}
                        filterOption='label'
                        maxVisible={10}
                        value={itemValue}
                        onKeyUp={onTypheadKeyPress}
                        displayOption={ (op) => op.label + ' (' + op.id + ')'}
                        onOptionSelected={onOptionSelected}
                    />
                }
            </div>
        </>
    )
}

export default PropertyWidget;