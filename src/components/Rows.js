import React, { useState } from 'react';
import Table from 'react-bootstrap/Table';

// API module import 
import wdSiteApi from './../api/wikidataSiteApi';

// Components import
import FormsCell from './FormsCell';
import SenseCell from './SenseCell';
import PropertyWidget from './PropertyWidget';
import EditorWidget from './editor/EditorWidget';

function Row( { index, item, type, properties } ) {
    // We need to modifiy the item state if users delete or edit it.
    const [ it, setIt ] = useState( item );

    const getPropsDataCell = (proId, proType) => {
        return(
            <td> 
                { 
                    it.claims[proId] !== undefined ? 
                    it.claims[proId].map( (i) => {
                        return( <tr>
                            <PropertyWidget d={i} key={i.id} it={it} setIt={setIt} proId={proId}/>
                        </tr>)
                    })
                    : null 
                }
                <EditorWidget 
                    proId={proId} 
                    proType={proType} 
                    setIt={setIt} 
                    it={it}
                />
            </td>
        )
    }

    const getTableRowBody = () => {
        if( type === 'form'){
            return(<td><Table><FormsCell data={it.forms} properties={properties} /></Table></td> )
        } else if ( type === 'sense' ){
            return(<td><Table><SenseCell data={it.senses} properties={properties} /></Table></td> )
        } else if( type === 'property'){
            return( <>{properties.map( (pro) => getPropsDataCell(pro.id, pro.type) )}</> )
        }
        // Removed me: P5402 P1552
    }

    return (
        <tr key={index}>
            <td>
                {index + 1}
            </td>
            <td>
                <a 
                    href={wdSiteApi.defaults.baseURL + "iki/Lexeme:" + it.id}
                    target="_blank"
                    rel="noreferrer"
                >
                    {it.lemmas[Object.keys(it.lemmas)[0]].value}
                </a>
                <br />
                <span className="lexemeId">({it.id})</span>
            </td>
            {getTableRowBody()}
        </tr>
    );
}

export default Row;