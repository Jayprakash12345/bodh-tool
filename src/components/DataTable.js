import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';
import CircularProgress from '@material-ui/core/CircularProgress';

// API module import
import wdSiteApi from './../api/wikidataSiteApi';

// Component import
import Row from './Rows';

// TODO: Convert class component to function component
class DataTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            type: null,
            properties: [],
            serialIndex: null,
            data: null
        };
    }

    // TODO: Replace with getDerivedStateFromProps
    componentWillReceiveProps(newProps) {
        this.setState( {
            ...this.state, 
            items: newProps.items,
            type: newProps.type,
            properties: newProps.properties,
            serialIndex: newProps.serialIndex
        });
    }

    componentDidUpdate(prev) {
        if( this.state.items.length > 0 && prev.items !== this.state.items ){
            wdSiteApi.get('/api.php?action=wbgetentities&format=json&origin=*&ids=' + this.state.items.join('|') )
                .then(( { data }) => this.setState( { ...this.state, data: Object.values( data.entities) } ) )
                .catch( ()=> alert("Input text is invaild. Go back and check again!") )
        }
    }

    // Return Property name also with P number
    getPropsHeaderName = ( pro ) => {
        try{
            return <p style={{padding:0, margin: 0}}>{pro.id}<span className="pId"> ({pro.text})</span></p>
        } catch {
            return pro.id
        }
    }

    getTableHeader = () => {
        if( this.state.type === 'form'){
            return( <><th>Form</th></> )
        } else if ( this.state.type === 'sense' ){
            return( <><th>Sense</th></> )
        } else if( this.state.type === 'property'){
            return( <>{this.state.properties.map( (pro) => <th>{this.getPropsHeaderName(pro)}</th>)}</> )
        }
    }

    render() {
        return (
            <>
            { this.state.items.length > 0 ?
                <Table bordered hover size="sm" style={{ fontSize: '13px'}}>
                    <thead>
                        <tr>
                            <th >#</th>
                            <th>Lexeme</th>
                            {this.getTableHeader()}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data ? this.state.data.map((item, index) => (
                            <Row index={this.state.serialIndex+index} 
                                item={item} 
                                type={this.state.type} 
                                properties={this.state.properties}
                            />
                        )) : 'Loading'}
                    </tbody>
                </Table>
                : <CircularProgress color="inherit" />
            }
            </>
        )
    }
}

export default DataTable;