import './App.css';
import React, { useState, useEffect } from 'react';

// Material-UI component
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import TabPanel from './components/TabPanel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Chip from '@material-ui/core/Chip';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import AddIcon from '@material-ui/icons/Add';

// API module import
import WikiApi, { allowProperties } from './api/wikiApi';
import backendApi from './api/backendApi';
import wdSiteApi from './api/wikidataSiteApi';

// Component import
import DataTable from './components/DataTable';

function a11yProps(index) {
	return {
		id: `vertical-tab-${index}`,
		'aria-controls': `vertical-tabpanel-${index}`,
	};
}

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.paper,
		display: 'flex',
		height: 200,
	},
	tabs: {
		borderRight: `1px solid ${theme.palette.divider}`,
	},
}));

// Initiating wdApi
const wdApi = new WikiApi();

function App() {
	const classes = useStyles();

	// Input mode states
	const [inputModeTab, setInputModeTab] = useState(0);
	const [sp_text, setSpText] = useState(`SELECT ?lexeme ?lemma ?sense ?sensedef {
	?lexeme wikibase:lemma ?lemma ; dct:language wd:Q9610 ; ontolex:sense ?sense.
	?sense skos:definition ?sensedef. FILTER(?sensedef = "শিব"@bn).
}`); // SPARQL text
	const [ml_text, setMlText] = useState('L1467'); // Manual text
	const [lexItems, setLexItems] = useState([]);

	// User auth states
	const [currentUser, setCurrentUser] = useState(null);

	// "Want to work" states, default: property (Lexeme statements)
	const [workOn, setWorkOn] = useState('property');
	const [columns, setColumns] = React.useState([{
		id: "P82",
		text: "instance of",
		type: "wikibase-item"
	},
	{
		id: "P74993",
		text: "lexeme prop",
		type: "wikibase-lexeme"
	}]);
	const [tempColText, setTempColText] = React.useState('');

	// Pagination
	const [currentPagination, setCurrentPaginaion] = useState(1)
	const [serialIndex, setSerialIndex] = useState(0)

	// To manage "Back to home" button state
	const [progress, setProgress] = React.useState(false);

	// Tab change handler
	const handleTabChange = (e, newValue) => {
		setInputModeTab(newValue);
	};

	// Handler for query submit button
	const handleSubmit = () => {
		setProgress(true);
		if (inputModeTab === 0) {
			if (sp_text === '') alert("SPARQL query text can't be empty!")
			wdApi.sparql_items(sp_text, function (i) {
				setLexItems(i);
			});
		} else if (inputModeTab === 1) {
			if (ml_text === '') alert("Lexeme list text can't be empty!")
			const a =  async () => {
				return ml_text.split('\n').map(function (i) {
						if (i.trim() !== "") {
							return i.trim();
						}
						return null
					}).filter(Boolean)
			}
			a().then( d => setLexItems(d) );
		}
	};

	// Handle Enter key for column adding
	const addPropertyColumn = (e) => {
		if (e.key === 'Enter') {
			if (columns.indexOf(tempColText) === -1) {
				wdSiteApi.get('/api.php?action=wbgetentities&format=json&origin=*&ids=' + tempColText)
					.then(( { data }) => {
						// Check whether this property is allowed or not
						if( allowProperties(data.entities[tempColText].datatype) === false) {
							alert("Tool does not support this property right now :(")
							return
						}

						let labels = data.entities[tempColText].labels
						let dataType = data.entities[tempColText].datatype
						try {
							setColumns([
								...columns,
								{
									id: tempColText,
									text: labels.en.value,
									type: dataType
								}
							])
						} catch {
							setColumns([
								...columns,
								{
									id: tempColText,
									text: labels.Object.keys(labels)[0].value,
									type: dataType
								}
							])
						}
					})
					.catch( () => {
						alert("Failed to added. Please make sure it starts with P");
					})
			}
			setTempColText('');
		}
	}

	// Handle + icon click for column adding
	// TODO: Merge with addPropertyColumn function
	const addPropertyColumnByButton = () => {
		if (columns.indexOf(tempColText) === -1) {
			wdSiteApi.get('/api.php?action=wbgetentities&format=json&origin=*&ids=' + tempColText)
				.then(( { data }) => {
					// Check whether this property is allowed or not
					if( allowProperties(data.entities[tempColText].datatype) === false) {
						alert("Tool does not support this property right now :(")
						return
					}

					let labels = data.entities[tempColText].labels
					let dataType = data.entities[tempColText].datatype
					try {
						setColumns([
							...columns,
							{
								id: tempColText,
								text: labels.en.value,
								type: dataType
							}
						])
					} catch {
						setColumns([
							...columns,
							{
								id: tempColText,
								text: labels.Object.keys(labels)[0].value,
								type: dataType
							}
						])
					}
				})
				.catch( () => {
					alert("Failed to added. Please make sure it starts with P");
				})

			setTempColText('');
		}
	}

	// Handle deleting column button
	const delPropertyColumn = (i) => {
		setColumns(columns.filter((item, index) => index !== i))
	}

	// To divide whole query items in 50 batch
	const paginator = () => {
		let page = currentPagination,
			per_page = 50,
			offset = (page - 1) * per_page,

			paginatedItems = lexItems.slice(offset).slice(0, per_page),
			total_pages = Math.ceil(lexItems.length / per_page);
		return {
			page: page,
			per_page: per_page,
			pre_page: page - 1 ? page - 1 : null,
			next_page: (total_pages > page) ? page + 1 : null,
			total: lexItems.length,
			total_pages: total_pages,
			data: paginatedItems
		};
	}

	useEffect(() => {
		// Checking whether user is authencated on backed or not
		backendApi.get("/api/profile")
			.then( ( { data }) => {
				if( data.logged ){
					setCurrentUser(data.username)
				}
			})
			.catch( () => alert("Something went wrong with auth."))
	}, [])

	return (
		<>
			<AppBar position="static" titleStyle={{ lineHeight: 'normal' }}>
				<Toolbar>
					<div style={{display: 'flex', flexDirection: 'column'}}>
						<Typography variant="h4" style={{padding: 0}}>
							<a href="/" style={{ color: 'white', textDecoration: 'none' }}>bodh</a>
						</Typography>
						<span style={{fontSize: '0.7rem', margin: 0,padding: 0}}>A tool that add sense to lexeme</span>
					</div>
					<div style={{ marginLeft: 'auto' }}>
						{currentUser ?
							<>
								<Typography style={{ display: "inline" }}>Logged in as {currentUser}   </Typography>
								<Button variant="contained" color="secondary" style={{ backgroundColor: 'orange' }}>
									<a href="https://bodh-backend.toolforge.org/logout" style={{ color: 'white' }}>Logout</a>
								</Button>
							</>
							:
							<Button variant="contained" color="secondary" style={{ backgroundColor: 'orange' }}>
								<a href="https://bodh-backend.toolforge.org/login" style={{ color: 'white' }}>Login</a>
							</Button>
						}
					</div>
				</Toolbar>
			</AppBar>
			<div className="App" style={{ padding: 30 }}>
				{progress === false ?
					<div>
						<div className={classes.root}>
							<Tabs orientation="vertical" value={inputModeTab} onChange={handleTabChange} className={classes.tabs}>
								<Tab label="SPARQL query" {...a11yProps(0)} />
								<Tab label="Manual lexeme list"  {...a11yProps(1)} />
							</Tabs>
							<TabPanel value={inputModeTab} index={0}>
								<TextField
									style={{ padding: 0, width: 800 }}
									multiline rows={8}
									rowsMax={8}
									variant="outlined"
									value={sp_text}
									placeholder="Enter the SPARQL query here"
									onChange={(e) => setSpText(e.target.value)}
								/>
							</TabPanel>
							<TabPanel value={inputModeTab} index={1}>
								<TextField
									style={{ padding: 0, width: 800 }}
									multiline rows={8}
									rowsMax={8}
									variant="outlined"
									value={ml_text}
									placeholder="Enter the lexemes list"
									onChange={(e) => setMlText(e.target.value)}
								/>
							</TabPanel>
						</div>
						<br />
						<div style={{ marginLeft: 10 }}>
							<FormLabel component="legend">I want to work on: </FormLabel>
							<RadioGroup row value={workOn} onChange={(e) => { setWorkOn(e.target.value); setColumns([]) }}>
								<FormControlLabel value="property" control={<Radio />} label="Lexeme statements" />
								<FormControlLabel value="sense" control={<Radio />} label="Senses" />
								<FormControlLabel value="form" disabled control={<Radio />} label="Form" />
							</RadioGroup>
						</div>
						<div>
							<p>Columns:</p>
							{columns.map((item, i) => (
								<Chip
									label={item.id}
									key={i}
									onDelete={() => delPropertyColumn(i)}
								/>
							))}
							<input
								style={{ display: 'inline', marginLeft: 10 }}
								type="text" placeholder="P1234" size={8}
								onKeyPress={addPropertyColumn}
								value={tempColText}
								onChange={(e) => setTempColText(e.target.value)}
							/>

							<Button
								size="medium" style={{ paddingTop: 0, paddingLeft: 0 }}
								color="primary"
								onClick={addPropertyColumnByButton}
							>
								<AddIcon />
							</Button>

						</div>
						<br />
						<Button variant="contained" color="primary" onClick={handleSubmit}>Start</Button>
					</div>
					:
					<div>
						<Button variant="outlined" onClick={() => setProgress(false)}>Back to home</Button>
						<br />
						<br />
						<DataTable items={paginator().data} serialIndex={serialIndex} type={workOn} properties={columns} />
						<p>Showing {paginator().page} of {paginator().total_pages} pages</p>
						<ButtonGroup color="secondary" aria-label="outlined secondary button group">
							<Button 
								disabled={paginator().pre_page === null ? true : false} 
								onClick={() => { setCurrentPaginaion(paginator().pre_page); setSerialIndex(serialIndex - 50) }}
							>Previous</Button>
							<Button 
								disabled={paginator().next_page === null ? true : false} 
								onClick={() => { setCurrentPaginaion(paginator().next_page); setSerialIndex(serialIndex + 50) }}
							>Next</Button>
						</ButtonGroup>
					</div>
				}
			</div>
		</>
	);
}

export default App;
